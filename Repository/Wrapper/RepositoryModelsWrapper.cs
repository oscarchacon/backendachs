﻿using Contracts.Models;
using Entities;
using Repository.Interfaces;
using Repository.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Wrapper
{
    /// <summary>
    /// Clase Contenedor de Repositorios, que hace la inyección de dependencias para las diferentes clases repositoras.
    /// </summary>
    public class RepositoryModelsWrapper : IRepositoryModelsWrapper
    {
        private readonly RepositoryContext repository;

        IPostRepository post;

        ITagRepository tag;

        ICommentRepository comment;

        public RepositoryModelsWrapper(RepositoryContext repository)
        {
            this.repository = repository;
        }

        #region Post
        public IPostRepository Post
        {
            get
            {
                if (this.post == null)
                {
                    this.post = new PostRepository(this.repository);
                }
                return this.post;
            }
        }
        #endregion Post

        #region Tag
        public ITagRepository Tag
        {
            get
            {
                if (this.tag == null)
                {
                    this.tag = new TagRepository(this.repository);
                }

                return this.tag;
            }
        }
        #endregion Tag

        #region Comment
        public ICommentRepository Comment
        {
            get
            {
                if (this.comment == null)
                {
                    this.comment = new CommentRepository(this.repository);
                }
                return this.comment;
            }
        }
        #endregion Comment

        public void Save()
        {
            this.repository.SaveChanges();
        }
    }
}
