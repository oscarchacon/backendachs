﻿using Entities.Utils.Paged;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Repository.Utils
{
    // Using from: https://www.codingame.com/playgrounds/5363/paging-with-entity-framework-core
    // And https://gunnarpeipman.com/net/ef-core-paging/
    /// <summary>
    /// Clase estatica que ayuda a obtener los resultados para la paginacion o uso de un objeto paginacion.
    /// </summary>
    public static class RepositoryExtension
    {
        public static PagedResult<T> GetPaged<T>(this IQueryable<T> query,
                                         int page, int pageSize) where T : class
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = query.Count()
            };


            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Results = query.Skip(skip).Take(pageSize).ToList();

            return result;
        }

        public static IEnumerable<T> GetPagedList<T>(this IQueryable<T> query,
                                         int page, int pageSize) where T : class
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = query.Count()
            };


            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = (page - 1) * pageSize;
            result.Results = query.Skip(skip).Take(pageSize).ToList();

            return result.Results;
        }
    }
}

