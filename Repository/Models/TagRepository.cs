﻿using Contracts.Models;
using Entities;
using Entities.Models;
using Entities.Models.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Repository.Models
{
    public class TagRepository : RepositoryBase<Tag>, ITagRepository
    {
        public TagRepository(RepositoryContext repository) : base(repository) { }

        public void CreateTag(Tag tag)
        {
            tag.Id = new Guid();
            this.Create(tag);
        }

        public void DeleteTag(Tag tag)
        {
            this.Delete(tag);
        }

        public IEnumerable<Tag> GetAllTags()
        {
            var tagsFind = this.FindAll();

            tagsFind = tagsFind.OrderBy(post => post.Name);

            return tagsFind.ToList();
        }

        public Tag GetTagById(Guid Id)
        {
            var tagFind = this.FindByCondition(tag => tag.Id.Equals(Id))
                                    .DefaultIfEmpty(new Tag())
                                    .FirstOrDefault();
            return tagFind;
        }

        public void UpdateTag(Tag dbTag, Tag tag)
        {
            dbTag.Map(tag);
            this.Update(dbTag);
        }
    }
}
