﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Models
{
    [Table("Tag")]
    public class Tag : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Column]
        public string Name { get; set; }

        public IList<PostTag> PostTags { get; set; }
    }
}
