﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models.Extensions
{
    public static class PostExtensions
    {
        public static void Map(this Post dbPost, Post post)
        {
            dbPost.Title = post.Title;
            dbPost.Description = post.Description;
            dbPost.ImageURL = post.ImageURL;
            dbPost.Author = post.Author;
        }
    }
}
