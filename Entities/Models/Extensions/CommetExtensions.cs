﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models.Extensions
{
    public static class CommetExtensions
    {
        public static void Map (this Comment dbCommnet, Comment comment)
        {
            dbCommnet.Author = comment.Author;
            dbCommnet.Commentary = comment.Commentary;
        }
    }
}
