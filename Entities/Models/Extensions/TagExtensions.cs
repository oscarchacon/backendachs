﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Models.Extensions
{
    public static class TagExtensions
    {
        public static void Map (this Tag dbTag, Tag tag)
        {
            dbTag.Name = tag.Name;
        }
    }
}
