using System.Collections.Generic;

namespace Entities.Utils.Paged.Interfaces
{
    public interface IPagedResult<T> : IPagedResultBase
    {
        IEnumerable<T> Results { get; set; }
    }
}