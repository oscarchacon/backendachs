﻿using Contracts.Interfaces;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Models
{
    public interface ITagRepository : IRepositoryBase<Tag>
    {
        IEnumerable<Tag> GetAllTags();

        Tag GetTagById(Guid Id);

        void CreateTag(Tag post);

        void UpdateTag(Tag dbTag, Tag tag);

        void DeleteTag(Tag tag);
    }
}
