﻿using Contracts.Interfaces;
using Entities.Models;
using Entities.Utils.Paged.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Contracts.Models
{
    public interface IPostRepository : IRepositoryBase<Post>
    {
        IEnumerable<Post> GetAllPosts();

        IPagedResult<Post> GetAllPostPaged(int? page = null, int? pageSize = null, Guid? tagId = null);

        Post GetPostById(Guid Id);

        void CreatePost(Post post);

        void UpdatePost(Post dbPost, Post post);

        void DeletePost(Post post);
    }
}
