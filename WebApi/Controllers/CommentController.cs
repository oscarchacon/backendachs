﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessRules;
using Entities.Models;
using Entities.Utils;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    /// <summary>
    /// Clase Controlador para los Comentarios
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly PostBR postBR;
        public CommentController(PostBR postBR)
        {
            this.postBR = postBR;
        }

        /// <summary>
        /// Api que obtiene todos los Comentarios
        /// </summary>
        /// <returns>Lista de Comentarios</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Comment>), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Get()
        {
            try
            {
                var commentsFind = this.postBR.GetAllComments();
                if (commentsFind.IsListObjectNull() || commentsFind.IsEmptyListObject()) { return NoContent(); }

                return Ok(commentsFind);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que obtiene un comentario por medio de su id
        /// </summary>
        /// <param name="id">Id de Comentario</param>
        /// <returns>Objeto Comentario</returns>
        [HttpGet("{id}", Name = "CommentById")]
        [ProducesResponseType(typeof(Comment), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Get(Guid id)
        {
            try
            {
                var commentFind = this.postBR.GetCommentById(id);
                if (commentFind.IsEmptyObject() || commentFind.IsObjectNull()) { return NoContent(); }

                return Ok(commentFind);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que crea un nuevo registro de Comentario
        /// </summary>
        /// <param name="commentNew">Objeto nuevo Comentario</param>
        /// <returns>Objeto Comentario Creado</returns>
        [HttpPost]
        [ProducesResponseType(typeof(Comment), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Post([FromBody]Comment commentNew)
        {
            if (commentNew.IsObjectNull()) { return BadRequest(new { Message = "Comment object is null" }); }
            if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }
            try
            {
                this.postBR.CreateComment(commentNew);
                if (commentNew.IsEmptyObject()) { return BadRequest(new { Message = "Comment Object is not Created" }); }

                return CreatedAtRoute("CommentById", new { id = commentNew.Id }, commentNew);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que actualiza el Comentario por medio de su Id
        /// </summary>
        /// <param name="id">Id de Comentario</param>
        /// <param name="comment">Objeto Comentario con datos Actualizados</param>
        /// <returns>Objeto Comentario con datos actualizados</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Comment), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult Put(Guid id, [FromBody]Comment comment)
        {
            try
            {
                if (id.Equals(Guid.Empty)) { return BadRequest(new { Message = "Id is Empty" }); }
                if (comment.IsObjectNull()) { return BadRequest(new { Message = "Comment Object is Null" }); }
                if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }

                bool secuence = this.postBR.UdpdateComment(id, comment);

                if (!secuence) { return NotFound(); }

                return Ok(comment);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que elimina un comentario por medio de su Id
        /// </summary>
        /// <param name="id">Id de Comentario</param>
        /// <returns>Sin contenido, objeto eliminado</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(405)]
        [ProducesResponseType(500)]
        public IActionResult Delete(Guid id)
        {
            try
            {
                if (id.Equals(Guid.Empty)) { return BadRequest(new { Message = "Id is Empty" }); }

                bool secuence = this.postBR.DeleteComment(id);

                if (!secuence) { return StatusCode(405, new { Message = "Not allowed to delete Comment registry." }); }

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }
    }
}
