﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessRules;
using Entities.Models;
using Entities.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApi.Controllers
{
    /// <summary>
    /// Clase controlador para uso de los Posts
    /// </summary>
    [Route("api/[controller]")]
    [ApiController, Produces("application/json")]
    public class PostController : ControllerBase
    {
        private readonly PostBR postBR;
        public PostController(PostBR postBR)
        {
            this.postBR = postBR;
        }

        #region Post
        /// <summary>
        /// Api que obtiene todos los Posts
        /// </summary>
        /// <returns>Lista de Posts</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Post>), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Get()
        {
            try
            {
                var postsFind = this.postBR.GetAllPosts();
                if (postsFind.IsListObjectNull() || postsFind.IsEmptyListObject()) { return NoContent(); }

                return Ok(postsFind);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que obtiene un objeto de paginación con la lista de Posts
        /// </summary>
        /// <param name="page">Pagina Actual</param>
        /// <param name="pageSize">Elementos por pagina</param>
        /// <param name="tagId">Id de Tag</param>
        /// <returns>Objeto de paginación con la lista de posts</returns>
        [HttpGet("Paged")]
        [ProducesResponseType(typeof(IEnumerable<Post>), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult GetPaged(int? page, int? pageSize, Guid? tagId)
        {
            try
            {
                var postsFind = this.postBR.GetAllPostsPaged(page, pageSize, tagId);
                if (postsFind.IsObjectNull() || postsFind.Results.IsListObjectNull() || postsFind.Results.IsEmptyListObject())
                {
                    return NoContent();
                }

                return Ok(postsFind);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que obtiene un post por medio de su id
        /// </summary>
        /// <param name="id">Id de Post</param>
        /// <param name="details">Si necesita detalles</param>
        /// <returns>Objeto Post</returns>
        [HttpGet("{id}", Name = "PostById")]
        [ProducesResponseType(typeof(Post), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Get(Guid id, bool details)
        {
            try
            {
                var postFind = this.postBR.GetPostById(id, details);
                if (postFind.IsEmptyObject() || postFind.IsObjectNull()) { return NoContent(); }

                return Ok(postFind);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que crea un nuevo registro de Post
        /// </summary>
        /// <param name="postNew">Objeto nuevo Post</param>
        /// <returns>Objeto Post Creado</returns>
        [HttpPost]
        [ProducesResponseType(typeof(Post), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Post([FromBody]Post postNew)
        {
            if (postNew.IsObjectNull()) { return BadRequest(new { Message = "Post object is null" }); }
            if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }
            try
            {
                this.postBR.CreatePost(postNew);
                if (postNew.IsEmptyObject()) { return BadRequest(new { Message = "Post Object is not Created" }); }

                return CreatedAtRoute("PostById", new { id = postNew.Id }, postNew);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que actualiza el post por medio de su Id
        /// </summary>
        /// <param name="id">Id de Post</param>
        /// <param name="post">Objeto post con datos Actualizados</param>
        /// <returns>Objeto Post con datos actualizados</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Post), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult Put(Guid id, [FromBody]Post post)
        {
            try
            {
                if (id.Equals(Guid.Empty)) { return BadRequest(new { Message = "Id is Empty" }); }
                if (post.IsObjectNull()) { return BadRequest(new { Message = "Post Object is Null" }); }
                if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }

                bool secuence = this.postBR.UdpdatePost(id, post);

                if (!secuence) { return NotFound(); }

                return Ok(post);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que elimina un post por medio de su Id
        /// </summary>
        /// <param name="id">Id de Post</param>
        /// <returns>Sin contenido, objeto eliminado</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(405)]
        [ProducesResponseType(500)]
        public IActionResult Delete(Guid id)
        {
            try
            {
                if (id.Equals(Guid.Empty)) { return BadRequest(new { Message = "Id is Empty" }); }

                bool secuence = this.postBR.DeletePost(id);

                if (!secuence) { return StatusCode(405, new { Message = "Not allowed to delete Post registry." }); }

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        #endregion Post

        #region Tag

        /// <summary>
        /// Api que obtiene todos los Comentarios de Post
        /// </summary>
        /// <param name="id">Id de Post</param>
        /// <returns>Lista de Comentarios de Post</returns>
        [HttpGet("{id}/Comments")]
        [ProducesResponseType(typeof(IEnumerable<Comment>), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult GetComments([FromBody]Guid id)
        {
            try
            {
                var commentsFind = this.postBR.GetAllCommentsByPost(id);
                if (commentsFind.IsListObjectNull() || commentsFind.IsEmptyListObject()) { return NoContent(); }

                return Ok(commentsFind);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        #endregion Tag
    }
}