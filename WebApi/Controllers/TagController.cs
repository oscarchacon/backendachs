﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessRules;
using Entities.Models;
using Entities.Utils;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    /// <summary>
    /// Clase controlador para uso de los Tags
    /// </summary>
    [Route("api/[controller]")]
    [ApiController, Produces("application/json")]
    public class TagController : ControllerBase
    {
        private readonly PostBR postBR;
        public TagController(PostBR postBR)
        {
            this.postBR = postBR;
        }

        /// <summary>
        /// Api que obtiene todos los Tags
        /// </summary>
        /// <returns>Lista de Tags</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Tag>), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Get()
        {
            try
            {
                var tagsFind = this.postBR.GetAllTags();
                if (tagsFind.IsListObjectNull() || tagsFind.IsEmptyListObject()) { return NoContent(); }

                return Ok(tagsFind);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que obtiene un tag por medio de su id
        /// </summary>
        /// <param name="id">Id de Tag</param>
        /// <returns>Objeto Tag</returns>
        [HttpGet("{id}", Name = "TagById")]
        [ProducesResponseType(typeof(Tag), 200)]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Get(Guid id)
        {
            try
            {
                var tagFind = this.postBR.GetTagById(id);
                if (tagFind.IsEmptyObject() || tagFind.IsObjectNull()) { return NoContent(); }

                return Ok(tagFind);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que crea un nuevo registro de Tag
        /// </summary>
        /// <param name="tagNew">Objeto nuevo Tag</param>
        /// <returns>Objeto Tag Creado</returns>
        [HttpPost]
        [ProducesResponseType(typeof(Tag), 201)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(500)]
        public IActionResult Post([FromBody]Tag tagNew)
        {
            if (tagNew.IsObjectNull()) { return BadRequest(new { Message = "Tag object is null" }); }
            if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }
            try
            {
                this.postBR.CreateTag(tagNew);
                if (tagNew.IsEmptyObject()) { return BadRequest(new { Message = "Tag Object is not Created" }); }

                return CreatedAtRoute("TagById", new { id = tagNew.Id }, tagNew);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que actualiza el Tag por medio de su Id
        /// </summary>
        /// <param name="id">Id de Tag</param>
        /// <param name="tag">Objeto tag con datos Actualizados</param>
        /// <returns>Objeto tag con datos actualizados</returns>
        [HttpPut("{id}")]
        [ProducesResponseType(typeof(Tag), 200)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        [ProducesResponseType(500)]
        public IActionResult Put(Guid id, [FromBody]Tag tag)
        {
            try
            {
                if (id.Equals(Guid.Empty)) { return BadRequest(new { Message = "Id is Empty" }); }
                if (tag.IsObjectNull()) { return BadRequest(new { Message = "Tag Object is Null" }); }
                if (!ModelState.IsValid) { return BadRequest(new { Message = "Invalid model object" }); }

                bool secuence = this.postBR.UdpdateTag(id, tag);

                if (!secuence) { return NotFound(); }

                return Ok(tag);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }

        /// <summary>
        /// Api que elimina un Tag por medio de su Id
        /// </summary>
        /// <param name="id">Id de Tag</param>
        /// <returns>Sin contenido, objeto eliminado</returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        [ProducesResponseType(401)]
        [ProducesResponseType(403)]
        [ProducesResponseType(405)]
        [ProducesResponseType(500)]
        public IActionResult Delete(Guid id)
        {
            try
            {
                if (id.Equals(Guid.Empty)) { return BadRequest(new { Message = "Id is Empty" }); }

                bool secuence = this.postBR.DeleteTag(id);

                if (!secuence) { return StatusCode(405, new { Message = "Not allowed to delete Tag registry." }); }

                return NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { Message = $"Internal server error: {ex.Message}" });
            }
        }
    }
}
