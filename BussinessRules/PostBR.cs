﻿using Entities.Models;
using Entities.Utils;
using Entities.Utils.Paged.Interfaces;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessRules
{
    public class PostBR
    {
        private readonly IRepositoryModelsWrapper repository;
        public PostBR(IRepositoryModelsWrapper repository)
        {
            this.repository = repository;
        }

        #region Post
        /// <summary>
        /// Metodo que obtiene todos los Posts
        /// </summary>
        /// <returns>Lista de Posts</returns>
        public IEnumerable<Post> GetAllPosts()
        {
            try
            {
                var postsFind = this.repository.Post.GetAllPosts();
                return postsFind;
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que obtiene un objeto de paginación con una lista de Posts
        /// </summary>
        /// <param name="page">Pagina Actual</param>
        /// <param name="pageSize">Elementos por Pagina</param>
        /// <param name="tagId">Id de Tag</param>
        /// <returns>Objeto de paginación con lista de Posts</returns>
        public IPagedResult<Post> GetAllPostsPaged(int? page = null, int? pageSize = null, Guid? tagId = null)
        {
            try
            {
                var postsFind = this.repository.Post.GetAllPostPaged(page, pageSize, tagId);
                return postsFind;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que obtiene un post por medio de su Id
        /// </summary>
        /// <param name="Id">Id de Post</param>
        /// <param name="details">Si necesita detalles del post</param>
        /// <returns>Objeto Post</returns>
        public Post GetPostById (Guid Id, bool details = false)
        {
            try
            {
                var postFind = this.repository.Post.GetPostById(Id);
                if (details)
                {
                    if (!postFind.IsEmptyObject())
                    {
                        var commentsFind = this.repository.Comment.GetCommentsByPost(postFind.Id);
                        if (!commentsFind.IsEmptyListObject() && !commentsFind.IsListObjectNull())
                        {
                            postFind.Comments = commentsFind.ToList();
                        }
                    }
                }
                return postFind;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que crea un nuevo Post
        /// </summary>
        /// <param name="postNew">Objeto Post nuevo</param>
        public void CreatePost(Post postNew)
        {
            try
            {
                this.repository.Post.CreatePost(postNew);
                this.repository.Save();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que actualiza los datos de un objeto post
        /// </summary>
        /// <param name="postId">Id de Post</param>
        /// <param name="postUpdated">Objeto Post con datos actualizados</param>
        /// <returns>Booleano si se actualizo el Objeto</returns>
        public bool UdpdatePost(Guid postId, Post postUpdated)
        {
            try
            {
                var dbPost = this.repository.Post.GetPostById(postId);
                if (dbPost.IsEmptyObject() || dbPost.IsObjectNull()) { return false; }

                this.repository.Post.UpdatePost(dbPost, postUpdated);
                this.repository.Save();

                postUpdated.Id = postId;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que elimina un objeto post por su Id
        /// </summary>
        /// <param name="postId">Id de Post</param>
        /// <returns>Booleano si se elimino Post</returns>
        public bool DeletePost(Guid postId)
        {
            try
            {
                var dbPost = this.repository.Post.GetPostById(postId);
                if (dbPost.IsEmptyObject()) { return false; }


                this.repository.Post.DeletePost(dbPost);
                this.repository.Save();

                return true;
            }
            catch (ArgumentNullException anex)
            {
                throw new Exception(anex.Message, anex.InnerException);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        #endregion Post

        #region Tag

        /// <summary>
        /// Metodo que obtiene todos los Tags
        /// </summary>
        /// <returns>Lista de Tags</returns>
        public IEnumerable<Tag> GetAllTags()
        {
            try
            {
                var tagsFind = this.repository.Tag.GetAllTags();
                return tagsFind;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que obtiene un tag por medio de su Id
        /// </summary>
        /// <param name="Id">Id de Tag</param>
        /// <returns>Objeto Tag</returns>
        public Tag GetTagById(Guid Id)
        {
            try
            {
                var tagFind = this.repository.Tag.GetTagById(Id);

                return tagFind;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que crea un nuevo Tag
        /// </summary>
        /// <param name="tagNew">Objeto Tag nuevo</param>
        public void CreateTag(Tag tagNew)
        {
            try
            {
                this.repository.Tag.CreateTag(tagNew);
                this.repository.Save();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que actualiza los datos de un objeto Tag
        /// </summary>
        /// <param name="tagId">Id de Tag</param>
        /// <param name="tagUpdated">Objeto Tag con datos actualizados</param>
        /// <returns>Booleano si se actualizo el Objeto</returns>
        public bool UdpdateTag(Guid tagId, Tag tagUpdated)
        {
            try
            {
                var dbTag = this.repository.Tag.GetTagById(tagId);
                if (dbTag.IsEmptyObject() || dbTag.IsObjectNull()) { return false; }

                this.repository.Tag.UpdateTag(dbTag, tagUpdated);
                this.repository.Save();

                tagUpdated.Id = tagId;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que elimina un objeto tag por su Id
        /// </summary>
        /// <param name="tagId">Id de Tag</param>
        /// <returns>Booleano si se elimino Tag</returns>
        public bool DeleteTag(Guid tagId)
        {
            try
            {
                var dbTag = this.repository.Tag.GetTagById(tagId);
                if (dbTag.IsEmptyObject()) { return false; }


                this.repository.Tag.DeleteTag(dbTag);
                this.repository.Save();

                return true;
            }
            catch (ArgumentNullException anex)
            {
                throw new Exception(anex.Message, anex.InnerException);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        #endregion Tag

        #region Comment

        /// <summary>
        /// Metodo que obtiene todos los Comentarios
        /// </summary>
        /// <returns>Lista de Comentarios</returns>
        public IEnumerable<Comment> GetAllComments()
        {
            try
            {
                var commentsFind = this.repository.Comment.GetAllComments();
                return commentsFind;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que obtiene los comentarios de un post
        /// </summary>
        /// <param name="postId">Id de Post</param>
        /// <returns>Lista de Comentarios</returns>
        public IEnumerable<Comment> GetAllCommentsByPost(Guid postId)
        {
            try
            {
                var commentsFind = this.repository.Comment.GetCommentsByPost(postId);
                return commentsFind;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que obtiene un Comentario por medio de su Id
        /// </summary>
        /// <param name="Id">Id de Comentario</param>
        /// <returns>Objeto Comentario</returns>
        public Comment GetCommentById(Guid Id)
        {
            try
            {
                var commentFind = this.repository.Comment.GetCommentById(Id);

                return commentFind;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que crea un nuevo Comentario
        /// </summary>
        /// <param name="commentNew">Objeto Comentario nuevo</param>
        public void CreateComment(Comment commentNew)
        {
            try
            {
                this.repository.Comment.CreateComment(commentNew);
                this.repository.Save();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que actualiza los datos de un objeto Comentario
        /// </summary>
        /// <param name="commentId">Id de Comentario</param>
        /// <param name="commentUpdated">Objeto Comentario con datos actualizados</param>
        /// <returns>Booleano si se actualizo el Objeto</returns>
        public bool UdpdateComment(Guid commentId, Comment commentUpdated)
        {
            try
            {
                var dbComment = this.repository.Comment.GetCommentById(commentId);
                if (dbComment.IsEmptyObject() || dbComment.IsObjectNull()) { return false; }

                this.repository.Comment.UpdateComment(dbComment, commentUpdated);
                this.repository.Save();

                commentUpdated.Id = commentId;

                return true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Metodo que elimina un objeto comentario por su Id
        /// </summary>
        /// <param name="commentId">Id de Comentario</param>
        /// <returns>Booleano si se elimino Comentario</returns>
        public bool DeleteComment(Guid commentId)
        {
            try
            {
                var dbComment = this.repository.Comment.GetCommentById(commentId);
                if (dbComment.IsEmptyObject()) { return false; }


                this.repository.Comment.DeleteComment(dbComment);
                this.repository.Save();

                return true;
            }
            catch (ArgumentNullException anex)
            {
                throw new Exception(anex.Message, anex.InnerException);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        #endregion Comment
    }


}
